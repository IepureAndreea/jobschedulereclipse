package com.fortech.tutorial;

import java.util.Date;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;

public class TriggerTest {
	public static void main(String args[]) {
        try{
            //Set job details.
            JobDetail job = new JobDetail();
            job.setName("myHelloJob");
            job.setJobClass(MyJob.class);

            //Set the scheduler timings.
            SimpleTrigger trigger = new SimpleTrigger();
            trigger.setName("simpleTrigger");
            trigger.setStartTime(new Date(System.currentTimeMillis()));

            //Job will be executed 5 times.
            trigger.setRepeatCount(5);

            // Execute the job after every 10 seconds.
            trigger.setRepeatInterval(10000);

            //Execute the job.
            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
            scheduler.scheduleJob(job, trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

    }

}
